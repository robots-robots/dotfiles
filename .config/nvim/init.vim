""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" vim-plug plugin manager
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if !exists('g:vscode')
	" Specify a directory for plugins
	call plug#begin('~/.config/nvim/bundle')

	" Code completion
	Plug 'neoclide/coc.nvim', {'branch': 'release'}
	Plug 'jiangmiao/auto-pairs'
"	Plug 'davidhalter/jedi-vim'
	Plug 'alvan/vim-closetag'
	  let g:closetag_close_shortcut = ''
	  let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.js,*jsx'
	Plug 'tpope/vim-commentary'

	" Syntax highlighting
	Plug 'othree/html5.vim'
	Plug 'hdima/python-syntax'
	Plug 'pangloss/vim-javascript'
	Plug 'hail2u/vim-css3-syntax'
	Plug 'sheerun/vim-polyglot'
	Plug 'norcalli/nvim-colorizer.lua'

	" Display
	Plug 'nathanaelkane/vim-indent-guides'
	Plug 'ntpeters/vim-better-whitespace'
	Plug 'kien/rainbow_parentheses.vim'
	Plug 'chiel92/vim-autoformat'
	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'
  Plug 'jeffkreeftmeijer/vim-numbertoggle' " Smart switch absolute/relative line numbers
  Plug 'dracula/vim', { 'as': 'dracula' }

	" Integrations
	Plug 'christoomey/vim-tmux-navigator', { 'for': 'tmux' }
	Plug 'tmux-plugins/vim-tmux', { 'for': 'tmux' }
	Plug 'scrooloose/nerdtree'
	Plug 'jmcantrell/vim-virtualenv', { 'for': 'python' }

	" Other Stuff
	Plug 'terryma/vim-multiple-cursors'

	" Initialize plugin system
	call plug#end()
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Neovim Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Detect unix, dos, mac file formats in that order
set fileformats=unix,dos,mac
" Remember more commands and search history
set history=1000
" Use many levels of undo
set undolevels=1000

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
" Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use ctrl-[hjkl] to select the active split!
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-l> :wincmd l<CR>

" Set :Prettier command to format code with coc-prettier
command! -nargs=0 Prettier :CocCommand prettier.formatFile

" Powerline fonts

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

let g:airline_theme='dracula'
let g:airline#extensions#tabline#enabled = 1 " Automatically displays all buffers when there's only one tab open

" Highlight trailing spaces and leading tabs
call matchadd('WarningMsg', '\s\+$')
call matchadd('WarningMsg', '^\t\+')

" statusline settings
let g:currentmode={
       \ 'n'  : 'NORMAL ',
       \ 'v'  : 'VISUAL ',
       \ 'V'  : 'V·Line ',
       \ "\<C-V>" : 'V·Block ',
       \ 'i'  : 'INSERT ',
       \ 'R'  : 'R ',
       \ 'Rv' : 'V·Replace ',
       \ 'c'  : 'Command ',
       \}

set statusline=
set statusline+=%1*
" Show current mode
set statusline+=\ %{toupper(g:currentmode[mode()])}
set statusline+=%{&spell?'[SPELL]':''}

set statusline+=%#WarningMsg#
set statusline+=%{&paste?'[PASTE]':''}

set statusline+=%2*
" File path, as typed or relative to current directory
set statusline+=\ %F

set statusline+=%{&modified?'\ [+]':''}
set statusline+=%{&readonly?'\ []':''}

" Truncate line here
set statusline+=%<

" Separation point between left and right aligned items.
set statusline+=%=

set statusline+=%{&filetype!=#''?&filetype.'\ ':'none\ '}

" Encoding & Fileformat
set statusline+=%#WarningMsg#
set statusline+=%{&fileencoding!='utf-8'?'['.&fileencoding.']':''}

set statusline+=%2*
set statusline+=%-7([%{&fileformat}]%)

set number relativenumber " Show vim-numbertoggle
set showmatch             " Show matching brackets.
