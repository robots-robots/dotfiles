# Baby's first dotfile repo

## How to migrate/setup

`alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'`

`echo ".cfg" >> .gitignore`

`git clone --bare git@bitbucket.org:robots-robots/dotfiles.git $HOME/.cfg`

### define the alias in the current scope

`alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'`

### checkout your dotfiles

`config checkout`

### if you run into duplicates and want to backup

``` bash
mkdir -p .cfg-backup && \
config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | \
xargs -I{} mv {} .cfg-backup/{}
```

### and checkout again

### then install "pure" zsh theme

``` bash
mkdir -p "$HOME/.zsh"
git clone https://github.com/sindresorhus/pure.git "$HOME/.zsh/pure"
```
